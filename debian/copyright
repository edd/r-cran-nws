This is the Debian GNU/Linux r-cran-nws package of nws which provides
NetWorkSpace (NWS) support for GNU R.  nws was written by REvolution
Computing <nws-support@revolution-computing.com> with support and
contributions from Pfizer, Inc.

This package was created by Dirk Eddelbuettel <edd@debian.org>. The
sources were downloaded from CRAN at 
	http://cran.r-project.org/src/contrib/

The package was renamed from its upstream name 'nws' to 'r-cran-nws' to 
fit the pattern of CRAN (and non-CRAN) packages for R.

Copyright (C) 2005 - 2008 REvolution Computing, Inc.

License: GPL (v2 or later)

On a Debian GNU/Linux system, the GPL licenses (versions 2 and 3) are
included in the files /usr/share/common-licenses/GPL-2 and
/usr/share/common-licenses/GPL-3.

For reference, the upstream DESCRIPTION [indented two spaces]
file is included below:

  Package: nws
  Title: R functions for NetWorkSpaces and Sleigh
  Version: 1.6.3
  Author: REvolution Computing <nws-support@revolution-computing.com>
          with support and contributions from Pfizer, Inc.
  Description: Provides coordination and parallel execution
          facilities, as well as limited cross-language data exchange,
          using the netWorkSpaces server developed by REvolution
          Computing
  Maintainer: REvolution Computing <nws-support@revolution-computing.com>
  License: GPL Version 2 or later.
  Depends: R (>= 2.1), methods
  LazyLoad: yes
  URL: http://nws-r.sourceforge.net/
  Packaged: Tue Mar 18 11:45:06 2008; weston
  
  